USE `container-db`;

DROP TABLE IF EXISTS tbl_arrangement;

CREATE TABLE tbl_arrangement (

    CustomerID INT(99) NOT NULL AUTO_INCREMENT,
    Name VARCHAR(50) NOT NULL,
    Mail VARCHAR(30) NOT NULL,
    Category VARCHAR(30),
    MESSAGE VARCHAR(255),
    PRIMARY KEY (CustomerID)

) AUTO_INCREMENT=1;


-- *********************************
-- ********* CREATE PEOPLE *********
-- *********************************

INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Vincent', 'nqt596927536@me.com','Travel','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Mike', '123@me.com','Wedding','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Josf', '321@me.com','Street style','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Dannie', '456@me.com','Portfolio','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Joana', '654@me.com','Family','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Lara', '789@me.com','Book Cover','Hello');
INSERT INTO tbl_arrangement (Name,Mail,Category,Message) VALUES ('Keith', '987@me.com','Cars','Hello');

SELECT * FROM tbl_arrangement ;

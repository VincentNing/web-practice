<!DOCTYPE html>

<html>

<head>
  <title>Penyler</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="layout/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="layout/styles/framework.css">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

</head>

<body>
  <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    <!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
-->
  </a>
  <div id="awcc1522988213910" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="en-us" data-useip="true"
    data-uid="awcc1522988213910"></div>
  <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>

  <div class="bgded overlay" style="background-image:url('images/placeholder.png');">
    <div class="wrapper">
      <header id="header" class="hoc clear">
        <div id="logo">
          <h1>
            <a href="index.html">Moment Pauser</a>
          </h1>
          <p>photographic company</p>
        </div>
        <nav id="site-nav">
          <button id="showTop"></button>
          <ul id="menu-main">
            <li id="menu-item-341">
              <a href="#">
                <span>Home</span>
              </a>
            </li>
            <li id="menu-item-342">
              <a href="pages/gallery.php">
                <span>Gallery</span>
              </a>
            </li>
            <li id="menu-item-448">
              <a href="pages/help.php">
                <span>Help</span>
              </a>
            </li>

          </ul>
        </nav>
        <nav id="mainav" class="clear">
          <ul class="clear">
            <li class="active">
              <a href="index.html">Home</a>
            </li>
            <li>
              <a href="pages/gallery.php">Gallery</a>
            </li>
            <li>
              <a href="pages/help.php">Help</a>
            </li>
          </ul>
        </nav>
      </header>
    </div>
    <div id="pageintro" class="hoc clear">
      <article>
        <em>Hello! My dear customer</em>
        <h2 class="heading">Welcome to Moment Pauser</h2>
        <p>It is nice to meet you here</p>
        <footer>
          <a class="btn" href="#">Start</a>
        </footer>
      </article>
    </div>
  </div>

  <div class="wrapper row3">
    <main class="hoc container clear">
      <ul class="nospace clear services">
        <li class="one_half first borderedbox">
          <div class="inspace-30">
            <h6 class="heading">About Us</h6>
            <p class="nospace">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur in orci viverra, malesuada ex quis, elementum
              orci. Donec pretium fermentum volutpat. Suspendisse venenatis sapien at augue vulputate, vel elementum massa
              rutrum. </p>
          </div>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image1</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image2</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia firstpic ">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image3</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia ">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image4</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image5</figcaption>
            </figure>
          </a>
        </li>
        <li class="one_quarter changingmedia">
          <a href="#">
            <figure>
              <img src="images/placeholder.png" alt="">
              <figcaption>image6</figcaption>
            </figure>
          </a>
        </li>
      </ul>

      <div class="clear"></div>
    </main>
  </div>
  <div class="wrapper bgded overlay" style="background-image:url('images/placeholder.png');">
    <article class="hoc container clear center">
      <h3 class="heading">Will be something here</h3>
      <p class="btmspace-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. </p>
      <a class="btn medium" href="#">Further Details</a>
    </article>
  </div>
  <div class="wrapper row3">
    <section class="hoc container clear">
      <div class="center btmspace-80">
        <h3 class="heading">Contact Us</h3>
        <p class="nospace ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus.</p>
        <div class="margin-top">
          <span class="font-x1">Phone</span>
          <br>
          <span class="font-x1">021-123321</span>
          <br>
        </div>
        <div class="margin-top">
          <span class="font-x1">E-mail</span>
          <br>
          <span class="font-x1">123321qweewq@qwe.com</span>
          <br>
        </div>

        <div class="margin-top">
          <span class="font-x1">Address</span>
          <br>
          <span class="font-x1">00,vincent St, City,Country</span>
          <br>
        </div>
      </div>
      <div id="map"></div>
      <script>
        function initMap() {
          var uluru = { lat: -37.685196, lng: 176.165765 };
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru
          });
          var marker = new google.maps.Marker({
            position: uluru,
            map: map
          });
        }
      </script>
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnv5k060UYf7fQ-Ullx93kZnL7WJfp_zA&callback=initMap">
      </script>
    </section>
  </div>

  <div class="bgded overlay" style="background-image:url('images/placeholder.png');">
    <footer id="footer" class="hoc clear center">
      <h3 class="heading uppercase">Moment Pauser</h3>
      <ul class="faico clear">
        <li>
          <a class="faicon-facebook" href="http://www.facebook.com">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a class="faicon-twitter" href="http://www.twitter.com">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li>
          <a class="faicon-dribble" href="http://www.dribbble.com">
            <i class="fab fa-dribbble"></i>
          </a>
        </li>
        <li>
          <a class="faicon-linkedin" href="http://nz.linkedin.com">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
        <li>
          <a class="faicon-google-plus" href="http://plus.google.com">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </li>
        <li>
          <a class="faicon-vk" href="http://www.vk.com">
            <i class="fab fa-vk"></i>
          </a>
        </li>
      </ul>
    </footer>
    <div id="copyright" class="hoc clear center">
      <p>Copyright &copy; 2018 - All Rights Reserved -
        <a href="#">Moment Pauser</a>
      </p>
    </div>
  </div>
  <a id="backtotop" href="#top">
    <i class="fa fa-chevron-up"></i>
  </a>
</body>
<script src="../layout/bootstrap/js/bootstrap.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
<script>
  function myFunction(x) {
    x.classList.toggle("change");
  }

  document.getElementById("showTop").addEventListener("click", function () {
    var display = document.getElementById("menu-main").style.display;
    document.getElementById("menu-main").style.display = (display == "none") ? "block" : "none";
  }, false);

</script>

</html>
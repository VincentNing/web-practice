<!DOCTYPE html>

<html>

<head>
  <title>Help</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="../layout/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../layout/styles/framework.css">
  <link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">

</head>

<body>

  <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    <!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
-->
  </a>
  <div id="awcc1522988213910" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="en-us" data-useip="true"
    data-uid="awcc1522988213910"></div>
  <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>

  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <div class="wrapper">
      <header id="header" class="hoc clear">
        <div id="logo">
          <h1>
            <a href="../index.html">Moment Pauser</a>
          </h1>
          <p>Photographic company</p>
        </div>
        <nav id="site-nav">
          <button id="showTop" class="active"></button>
          <ul id="menu-main">
            <li id="menu-item-341">
              <a href="../index.html">
                <span>Home</span>
              </a>
            </li>
            <li id="menu-item-342">
              <a href="gallery.html">
                <span>Gallery</span>
              </a>
            </li>
            <li id="menu-item-448">
              <a href="help.html">
                <span>Help</span>
              </a>
            </li>

          </ul>
        </nav>
        <nav id="mainav" class="clear">
          <ul class="clear">
            <li>
              <a href="../index.html">Home</a>
            </li>
            <li>
              <a href="gallery.html">Gallery</a>
            </li>
            <li class="active">
              <a href="help.html">Help</a>
            </li>
          </ul>
        </nav>
      </header>
    </div>
  </div>
  <!-- End Top Background Image Wrapper -->
  <div class="wrapper row3 ">
    <main class="hoc container clear">
      <!-- main body -->
      <div class="content hoc">

        <h1>Some Paragraphs</h1>
        <img class="imgr borderedbox inspace-5 insertedPic" src="../images/placeholder.png" alt="">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus.
          <a href="#">Useful Link</a> Ut sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse
          gravida lectus ac turpis tristique imperdiet. Vestibulum condimentum porttitor feugiat. Sed quis massa augue. Maecenas
          eget risus eu metus vehicula laoreet eu at elit.
          <a href="#">Useful Link</a> Fusce vel quam a lorem tincidunt pretium. Proin fermentum facilisis nulla a tincidunt. Class aptent
          taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi sit amet nulla tortor.
          <a
            href="#">Useful Link</a> Ut hendrerit orci id sem rhoncus semper. Vivamus ac ex in nibh euismod porttitor sit amet id lorem.
            Integer lacinia massa eget pretium pulvinar. Nam at nunc eros.</p>
        <img class="imgl borderedbox inspace-5 insertedPic" src="../images/placeholder.png" alt="">
        <p>Sed vel magna et leo laoreet elementum et vel nisl. Ut porta quam eget dictum ullamcorper.
          <a href="#">Useful Link</a> Nam ut tellus convallis, elementum felis vitae, semper ligula. Etiam mattis lacus facilisis lorem
          pulvinar varius. Nunc mi metus, luctus dapibus tortor in, vestibulum dapibus arcu. Quisque vehicula urna vitae
          viverra aliquam.
          <a href="#">Useful Link</a> Proin convallis nulla ullamcorper nisl convallis sagittis. Morbi luctus, metus nec pharetra lacinia,
          lectus risus rutrum tortor, sed vehicula ex dolor eu nunc.
          <a href="#">Useful Link</a> Cras odio lacus, pellentesque quis ligula eget, facilisis sagittis justo. Integer lobortis consectetur
          commodo. Suspendisse potenti. Proin et felis sed nibh faucibus euismod.</p>

      </div>
      <div class="clear"></div>
      <h3 class="padding-16 ">My Price</h3>
      <div class="row-padding" style="margin:0 -16px">
        <div class="gohalf x-margin-bottom">
          <ul class="x-ul  center x-opacity x-hover-opacity-off">
            <li class="x-dark-grey font-x3 padding-32">Basic</li>
            <li class="padding-16">Wedding</li>
            <li class="padding-16">Travel shots</li>
            <li class="padding-16">Portfolio</li>
            <li class="padding-16">Videos</li>
            <li class="padding-16">
              <h2>$ 30</h2>
              <span class="x-opacity">per hour</span>
            </li>
            <li class="x-light-grey x-padding-24">
              <button class="btn">chose</button>
            </li>
          </ul>
        </div>

        <div class="gohalf">
          <ul class="x-ul center x-opacity x-hover-opacity-off">
            <li class="dark-grey font-x3 padding-32">Pro</li>
            <li class="padding-16">Ultra Wedding</li>
            <li class="padding-16">Ultra Travel shots</li>
            <li class="padding-16">Ultra Portfolio</li>
            <li class="padding-16">ultra videos</li>
            <li class="padding-16">
              <h2>$ 55</h2>
              <span class="x-opacity">per hour</span>
            </li>
            <li class="padding-24">
              <button class="btn">chose</button>
            </li>
          </ul>
        </div>

      </div>
      <div class="row hoc">
        <div class="comments">
          <h2>FAQs</h2>
          <ul>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="../images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2045-07-06T08:15+00:00">Friday, 6
                    <sup>th</sup> July 2045 @08:15:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="../images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2321-06-14T04:55+00:00">Wednesday, 14
                    <sup>th</sup> June 2312 @04:55:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
            <li>
              <article>
                <header>
                  <figure class="avatar profile">
                    <img style="padding-left: 5px; padding-top: 5px;" src="../images/avatar.png" alt="">
                  </figure>
                  <address>
                    By
                    <a href="#">A Name</a>
                  </address>
                  <time datetime="2115-4-24T023:40+00:00">Monday, 24
                    <sup>th</sup> April 2115 @23:40:00</time>
                </header>
                <div class="comcont">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                    sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                    lectus ac turpis tristique imperdiet.</p>
                </div>
                <div>
                  <strong>Solution</strong>
                  <div class="comcont">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. Ut
                      sed vestibulum nulla, in blandit sem. Curabitur luctus facilisis velit quis ultrices. Suspendisse gravida
                      lectus ac turpis tristique imperdiet.</p>
                  </div>
                </div>
              </article>
            </li>
          </ul>
        </div>
      </div>

      <h3 class="heading">Make A Arrangement</h3>
      <div class="comments">
        <form action="#" method="post">
          <div class="one_third first">
            <label class="center" for="name">Name
              <span>*</span>
            </label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="email">Mail
              <span>*</span>
            </label>
            <input type="email" name="email" id="email" value="" size="22" required>
          </div>
          <div class="one_third">
            <label class="center" for="url">Category</label>
            <input type="url" name="url" id="url" value="" size="22">
          </div>
          <div class="block clear">
            <label class="center" for="comment">Message</label>
            <textarea name="comment" id="comment" cols="25" rows="10"></textarea>
          </div>
          <div class="center">
            <input type="submit" name="submit" value="Submit Form"> &nbsp;
            <input type="reset" name="reset" value="Reset Form">
          </div>
        </form>
      </div>

      <div class="clear"></div>
    </main>
  </div>
  <div class="wrapper bgded overlay" style="background-image:url('../images/placeholder.png');">
    <article class="hoc container clear center">
      <h3 class="heading">Polices</h3>
      <p class="btmspace-50">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu enim egestas est iaculis luctus. </p>
      <a class="btn medium" href="policy.html">Polices</a>
    </article>
  </div>
  <!-- / main body -->

  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <footer id="footer" class="hoc clear center">
      <h3 class="heading uppercase">Moment Pauser</h3>
      <ul class="faico clear">
        <li>
          <a class="faicon-facebook" href="http://www.facebook.com">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a class="faicon-twitter" href="http://www.twitter.com">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li>
          <a class="faicon-dribble" href="http://www.dribbble.com">
            <i class="fab fa-dribbble"></i>
          </a>
        </li>
        <li>
          <a class="faicon-linkedin" href="http://nz.linkedin.com">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
        <li>
          <a class="faicon-google-plus" href="http://plus.google.com">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </li>
        <li>
          <a class="faicon-vk" href="http://www.vk.com">
            <i class="fab fa-vk"></i>
          </a>
        </li>
      </ul>
    </footer>
    <div id="copyright" class="hoc clear center">
      <p>Copyright &copy; 2018 - All Rights Reserved -
        <a href="#">Moment Pauser</a>
      </p>
    </div>
  </div>
</body>
<script src="../layout/bootstrap/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7xmgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
<script>
  function myFunction(x) {
    x.classList.toggle("change");
  }

  document.getElementById("showTop").addEventListener("click", function () {
    var display = document.getElementById("menu-main").style.display;
    document.getElementById("menu-main").style.display = (display == "none") ? "block" : "none";
  }, false);

</script>

</html>
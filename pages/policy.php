<!DOCTYPE html>

<html>

<head>
  <title>Gallery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  <link rel="stylesheet" href="../layout/bootstrap/css/bootstrap.min.css">
  <link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <link rel="stylesheet" href="../layout/styles/framework.css">
</head>
<body id="top">
   <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    <!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
-->
  </a>
  <div id="awcc1522988213910" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="en-us" data-useip="true"
    data-uid="awcc1522988213910"></div>
  <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>
  
  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <div class="wrapper">
      <header id="header" class="hoc clear">
        <div id="logo">
          <h1>
            <a href="../index.html">Moment Pauser</a>
          </h1>
          <p>photographic company</p>
        </div>
        <nav id="site-nav">
 			<button id="showTop" class="active"></button>
 			<ul id ="menu-main" >
 				<li id="menu-item-341" ><a href="../index.html" ><span >Home</span></a></li>
<li id="menu-item-342" ><a href="gallery.html" ><span >Gallery</span></a></li>
<li id="menu-item-448" ><a href="help.html" ><span>Help</span></a></li>

 			</ul>
 		</nav>
        <nav id="mainav" class="clear">
          <ul class="clear">
            <li>
              <a href="../index.html">Home</a>
            </li>

            <li class="active">
              <a href="gallery.html">Gallery</a>
            </li>
            <li>
              <a href="help.html">Help</a>
            </li>

          </ul>
        </nav>
      </header>
    </div>
  </div>
  <!-- End Top Background Image Wrapper -->
<div class=container>
      <main class="row">
      <h1 class="center">Policy</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet enim a augue mollis viverra. Vivamus dignissim tellus et ultrices pharetra. Nam et enim sit amet nisl malesuada interdum. In hac habitasse platea dictumst. Donec hendrerit, sem eu sagittis cursus, ligula nisl laoreet neque, in lobortis purus odio at mauris. Aenean eu metus enim. Integer laoreet sed diam vel dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam venenatis, felis at gravida dapibus, ipsum velit ultrices nulla, eu faucibus sapien risus at nunc. Curabitur imperdiet lectus quis libero suscipit, nec dapibus enim laoreet. In eros orci, sollicitudin in nibh eu, blandit porttitor est.</p>
     <p>Donec eros lorem, dictum vel imperdiet sed, blandit sed sapien. Vivamus nisl libero, sodales id sagittis et, varius vel quam. Morbi eleifend dignissim eros quis placerat. Aenean ultricies metus turpis, at ultricies elit efficitur quis. Nam eu turpis lorem. Curabitur accumsan tincidunt ante a feugiat. Vestibulum augue velit, porttitor in placerat ut, euismod sit amet purus. Proin sit amet nulla sit amet lorem sollicitudin euismod. Quisque condimentum elementum orci, in posuere lorem faucibus a. Vivamus vitae vestibulum dolor, sit amet consectetur purus. Vestibulum vel mi mi. Proin feugiat, sapien quis malesuada gravida, ipsum est rhoncus augue, sed eleifend dolor metus eu diam. Aliquam sed sem sodales, semper odio eleifend, ornare felis. Cras tempus quam nec ligula gravida placerat sed a dolor.</p>
     <p>Morbi mattis, tellus id vulputate aliquet, ex nisl ornare justo, quis ultricies ligula nulla id massa. Mauris sed dolor at lectus auctor commodo vitae vel nulla. Quisque sodales purus hendrerit, egestas diam id, ullamcorper tortor. Donec sit amet turpis diam. Phasellus eget feugiat nisl. Nam scelerisque ornare ante, non volutpat ex eleifend ac. Aenean sollicitudin gravida ex et convallis.</p>
     <p>Nunc varius leo nec lobortis pharetra. Nullam pretium quam nunc, et ultrices dui laoreet ut. Cras nisl orci, interdum at rutrum sed, fringilla eu tortor. Suspendisse ornare orci non lectus accumsan, eget sagittis mi molestie. Donec et dolor cursus, sollicitudin eros ac, laoreet leo. Suspendisse tincidunt lectus a faucibus venenatis. Proin feugiat semper dui a posuere. Sed eleifend mollis sollicitudin. Mauris tristique euismod viverra. Vestibulum lorem ex, scelerisque quis magna ut, bibendum consequat sem. Fusce gravida orci a lectus posuere egestas.</p>
     <p>Etiam dui diam, auctor et porttitor vel, finibus quis orci. Fusce nunc odio, placerat et tortor in, venenatis ultricies ipsum. Nam porta sapien a enim interdum, ut euismod augue euismod. Phasellus rhoncus imperdiet mattis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed efficitur convallis felis a molestie. Sed in justo eu sapien fermentum semper.</p>
      <div class="clear"></div>
    </main>
  </div>
  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <footer id="footer" class="hoc clear center">
      <h3 class="heading uppercase">Moment Pauser</h3>
      <ul class="faico clear">
<li>
          <a class="faicon-facebook" href="http://www.facebook.com">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a class="faicon-twitter" href="http://www.twitter.com">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li>
          <a class="faicon-dribble" href="http://www.dribbble.com">
            <i class="fab fa-dribbble"></i>
          </a>
        </li>
        <li>
          <a class="faicon-linkedin" href="http://nz.linkedin.com">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
        <li>
          <a class="faicon-google-plus" href="http://plus.google.com">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </li>
        <li>
          <a class="faicon-vk" href="http://www.vk.com">
            <i class="fab fa-vk"></i>
          </a>
        </li>
      </ul>
    </footer>
    <div id="copyright" class="hoc clear center">
      <p>Copyright &copy; 2018 - All Rights Reserved -
        <a href="#">Moment Pauser</a>
      </p>

    </div>
  </div>
  <a id="backtotop" href="#top">
    <i class="fa fa-chevron-up"></i>
  </a>
</body>
<script src="../layout/bootstrap/js/bootstrap.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
function myFunction(x) {
    x.classList.toggle("change");
}
	
	document.getElementById("showTop").addEventListener("click",function(){
		var display = document.getElementById("menu-main").style.display;
		document.getElementById("menu-main").style.display = (display == "none") ? "block" : "none";
	}, false);
	
</script>
</html>
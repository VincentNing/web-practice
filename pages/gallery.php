<!DOCTYPE html>

<html>

<head>
  <title>Gallery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="../layout/styles/framework.css">
  <link rel="stylesheet" href="../layout/bootstrap/css/bootstrap.min.css">

  <link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>

<body id="top">
  <a href="https://www.accuweather.com/en/nz/tauranga/246959/weather-forecast/246959" class="aw-widget-legal">
    <!--
By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at https://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at https://www.accuweather.com/en/privacy.
-->
  </a>
  <div id="awcc1522988213910" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="en-us" data-useip="true"
    data-uid="awcc1522988213910"></div>
  <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>

  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <div class="wrapper">
      <header id="header" class="hoc clear">
        <div id="logo">
          <h1>
            <a href="../index.html">Moment Pauser</a>
          </h1>
          <p>photographic company</p>
        </div>
        <nav id="site-nav">
          <button id="showTop" class="active"></button>
          <ul id="menu-main">
            <li id="menu-item-341">
              <a href="../index.html">
                <span>Home</span>
              </a>
            </li>
            <li id="menu-item-342">
              <a href="gallery.html">
                <span>Gallery</span>
              </a>
            </li>
            <li id="menu-item-448">
              <a href="help.html">
                <span>Help</span>
              </a>
            </li>

          </ul>
        </nav>
        <nav id="mainav" class="clear">
          <ul class="clear">
            <li>
              <a href="../index.html">Home</a>
            </li>

            <li class="active">
              <a href="gallery.html">Gallery</a>
            </li>
            <li>
              <a href="help.html">Help</a>
            </li>

          </ul>
        </nav>
      </header>
    </div>
  </div>

  <div class="wrapper row3">
    <main class="hoc container clear">

      <div class="content">
        <div id="gallery">
          <figure>
            <header class="heading">Gallery Title Goes Here</header>
            <ul class="nospace clear">
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia firstpic">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
              <li class="one_quarter changingmedia">
                <a href="#">
                  <img src="../images/placeholder.png" alt="">
                </a>
              </li>
            </ul>
            <figcaption>Gallery Description Goes Here</figcaption>
          </figure>
        </div>
        <nav class="pagination">
          <ul>
            <li>
              <a href="#">&laquo; Previous</a>
            </li>
            <li>
              <a href="#">1</a>
            </li>

            <li>
              <strong>&hellip;</strong>
            </li>
            <li>
              <a href="#">6</a>
            </li>
            <li class="current">
              <strong>7</strong>
            </li>
            <li>
              <a href="#">8</a>
            </li>

            <li>
              <strong>&hellip;</strong>
            </li>

            <li>
              <a href="#">15</a>
            </li>
            <li>
              <a href="#">Next &raquo;</a>
            </li>
          </ul>
        </nav>
      </div>

      <div class="clear"></div>
    </main>
  </div>
  <div class="bgded overlay" style="background-image:url('../images/placeholder.png');">
    <footer id="footer" class="hoc clear center">
      <h3 class="heading uppercase">Moment Pauser</h3>
      <ul class="faico clear">
        <li>
          <a class="faicon-facebook" href="http://www.facebook.com">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a class="faicon-twitter" href="http://www.twitter.com">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li>
          <a class="faicon-dribble" href="http://www.dribbble.com">
            <i class="fab fa-dribbble"></i>
          </a>
        </li>
        <li>
          <a class="faicon-linkedin" href="http://nz.linkedin.com">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
        <li>
          <a class="faicon-google-plus" href="http://plus.google.com">
            <i class="fab fa-google-plus-g"></i>
          </a>
        </li>
        <li>
          <a class="faicon-vk" href="http://www.vk.com">
            <i class="fab fa-vk"></i>
          </a>
        </li>
      </ul>
    </footer>
    <div id="copyright" class="hoc clear center">
      <p>Copyright &copy; 2018 - All Rights Reserved -
        <a href="#">Moment Pauser</a>
      </p>

    </div>
  </div>
  <a id="backtotop" href="#top">
    <i class="fa fa-chevron-up"></i>
  </a>
</body>
<script src="../layout/bootstrap/js/bootstrap.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl"
  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
  crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
  crossorigin="anonymous"></script>
<script>
  function myFunction(x) {
    x.classList.toggle("change");
  }

  document.getElementById("showTop").addEventListener("click", function () {
    var display = document.getElementById("menu-main").style.display;
    document.getElementById("menu-main").style.display = (display == "none") ? "block" : "none";
  }, false);

</script>

</html>